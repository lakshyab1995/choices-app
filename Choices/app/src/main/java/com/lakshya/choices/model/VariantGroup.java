package com.lakshya.choices.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class VariantGroup implements Serializable {

    @SerializedName("group_id")
    @Expose
    private String groupId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("variations")
    @Expose
    private List<Variant> variants;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Variant> getVariants() {
        return variants;
    }

    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

}
