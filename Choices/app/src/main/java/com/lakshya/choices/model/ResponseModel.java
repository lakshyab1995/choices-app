package com.lakshya.choices.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseModel implements Serializable {

    @SerializedName("variants")
    @Expose
    private VariantResponse response;

    public VariantResponse getResponse() {
        return response;
    }

    public void setResponse(VariantResponse response) {
        this.response = response;
    }
}
