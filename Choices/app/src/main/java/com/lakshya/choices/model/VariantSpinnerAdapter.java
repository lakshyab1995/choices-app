package com.lakshya.choices.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lakshya.choices.R;

import java.util.Iterator;
import java.util.List;

public class VariantSpinnerAdapter extends BaseAdapter {

    private Context context;
    private List<Variant> variants;

    public VariantSpinnerAdapter(Context context, VariantGroup group) {
        this.context = context;
        this.variants = group.getVariants();
    }

    @Override
    public int getCount() {
        return variants.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.variant_list_item, parent, false);
            viewHolder.variantName = convertView.findViewById(R.id.nameStock);
            viewHolder.variantPrice = convertView.findViewById(R.id.price);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.variantName.setText(variants.get(position).getName() + "-" + variants.get(position).getStock());
        viewHolder.variantPrice.setText(String.valueOf(variants.get(position).getPrice()));
        return convertView;
    }

    private static class ViewHolder {

        private TextView variantName;
        private TextView variantPrice;
    }
}
