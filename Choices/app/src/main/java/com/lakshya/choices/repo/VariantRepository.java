package com.lakshya.choices.repo;

import android.arch.lifecycle.MutableLiveData;

import com.lakshya.choices.api.RestAdapter;
import com.lakshya.choices.model.ResponseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VariantRepository {

    private static VariantRepository variantRepository;
    private RestAdapter adapter;

    public static VariantRepository getInstance() {
        if (variantRepository == null) {
            variantRepository = new VariantRepository();
        }
        return variantRepository;
    }

    private VariantRepository() {
        adapter = new RestAdapter();
    }

    public MutableLiveData<ResponseModel> getVariants(){
        final MutableLiveData<ResponseModel> variantData = new MutableLiveData<>();
        adapter.getApiClient().fetchVariants().enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if(response.isSuccessful()){
                    variantData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                variantData.setValue(null);
            }
        });
        return variantData;
    }
}
