package com.lakshya.choices;

import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lakshya.choices.model.ExcludeVariant;
import com.lakshya.choices.model.VariantGroup;
import com.lakshya.choices.model.VariantSpinnerAdapter;
import com.lakshya.choices.viewmodel.VariantViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    VariantViewModel variantViewModel;
    LinearLayout parentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*LayoutInflater layoutInflater =
                (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.activity_main, null);*/
        parentLayout = findViewById(R.id.rootView);
        variantViewModel = ViewModelProviders.of(this).get(VariantViewModel.class);
        variantViewModel.init();
        variantViewModel.getVariants().observe(this, variantResponse -> {
            if (variantResponse != null) {
                List<List<ExcludeVariant>> exclusionList = variantResponse.getResponse().getExcludeList();
                List<VariantGroup> groups = variantResponse.getResponse().getVariantGroups();
                for (VariantGroup group : groups) {
                    constructGroupLayout(group, exclusionList);
                }
            }
        });
    }

    private void constructGroupLayout(VariantGroup group, List<List<ExcludeVariant>> exclusionList) {
        LinearLayout.LayoutParams groupLayoutParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout groupLayout = new LinearLayout(this);
        groupLayout.setOrientation(LinearLayout.VERTICAL);
        groupLayout.setPadding(10, 15, 10, 15);
        groupLayout.setGravity(Gravity.CENTER);
        groupLayout.setLayoutParams(groupLayoutParams);
        TextView groupTitle = new TextView(this);
        groupTitle.setTextColor(Color.BLACK);
        groupTitle.setText(group.getName());
        groupTitle.setTextSize(15);
        Spinner groupList = new Spinner(this);
        groupList.setBackgroundResource(R.drawable.spinner_bg);
        groupList.setElevation(3);
        groupList.setStateListAnimator(AnimatorInflater.loadStateListAnimator(this, R.drawable.spinner_sla));
        LinearLayout.LayoutParams spinnerParams =
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        spinnerParams.setMarginStart(5);
        groupList.setLayoutParams(spinnerParams);
        groupList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                /*for(List<ExcludeVariant> exculde : exclusionList){
                    //exculde.
                    for(ExcludeVariant var : exculde){
                        if(group.getGroupId().equals(var.getGroupId()) && group.getVariants().get(position).getId().equals(var.getVariationId())){
                            Toast.makeText(getApplicationContext(), "Match",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }*/
                /*Toast.makeText(getApplicationContext(), group.getVariants().get(position).getName(),
                        Toast.LENGTH_SHORT).show();*/
                /*TextView errorText = (TextView)groupList.getChildAt(-1);
                errorText.setError("");
                errorText.setTextColor(Color.RED);
                errorText.setText("my actual error text");*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        groupList.setAdapter(new VariantSpinnerAdapter(this, group));
        groupLayout.addView(groupTitle, 0);
        groupLayout.addView(groupList, -1);
        parentLayout.addView(groupLayout);
    }
}
