package com.lakshya.choices.api;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestAdapter {

    private static final String BASE_URL = "https://api.myjson.com/";

    private static ApiClient mApiClient;

    public RestAdapter(){
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        OkHttpClient okHttpClient = builder.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mApiClient = retrofit.create(ApiClient.class);
    }

    public ApiClient getApiClient(){
        return  mApiClient;
    }
}
