package com.lakshya.choices.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class VariantResponse implements Serializable {

    @SerializedName("variant_groups")
    @Expose
    private List<VariantGroup> variantGroups;

    @SerializedName("exclude_list")
    @Expose
    private List<List<ExcludeVariant>> excludeList;

    public List<VariantGroup> getVariantGroups() {
        return variantGroups;
    }

    public void setVariantGroups(List<VariantGroup> variantGroups) {
        this.variantGroups = variantGroups;
    }

    public List<List<ExcludeVariant>> getExcludeList() {
        return excludeList;
    }

    public void setExcludeList(List<List<ExcludeVariant>> excludeList) {
        this.excludeList = excludeList;
    }
}
