package com.lakshya.choices.api;

import com.lakshya.choices.model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiClient {

    @GET("bins/19u0sf")
    Call<ResponseModel> fetchVariants();
}
