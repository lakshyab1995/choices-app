package com.lakshya.choices.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.lakshya.choices.model.ResponseModel;
import com.lakshya.choices.repo.VariantRepository;

public class VariantViewModel extends ViewModel {

    private MutableLiveData<ResponseModel> mutableLiveData;
    private VariantRepository variantRepository;

    public void init(){
        if (mutableLiveData != null) {
            return;
        }
        variantRepository = VariantRepository.getInstance();
        mutableLiveData = variantRepository.getVariants();
    }

    public LiveData<ResponseModel> getVariants(){
        return mutableLiveData;
    }
}
